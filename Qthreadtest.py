from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QPushButton
from PyQt5.QtCore import QThread, QMutex, QWaitCondition

class Worker(QThread):
    def definenet(self, name, waitcond):
        self.name = name
        print("here we define")
        self.waitcond = waitcond
        self.mutex = QMutex()
    
    def inittf(self):
        print("TF initialized")

    def run(self):
        self.inittf()
        while True:
            self.mutex.lock()
            self.waitcond.wait(self.mutex)
            print("I'm alive{}".format(self.name))
            self.mutex.unlock()

if __name__ == "__main__":
    signaller = QWaitCondition()
    app = QApplication([])
    window = QWidget()
    layout = QVBoxLayout()

    workers = []
    for i in range(3):
        workers.append(Worker())

    for i, w in enumerate(workers):
        w.definenet(str(i), signaller)
        w.start()
        
    button = QPushButton(str(i))
    button.clicked.connect(signaller.wakeAll)
    layout.addWidget(button)
    window.setLayout(layout)
    window.show()
    app.exec_()
        
