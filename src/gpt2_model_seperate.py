from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QPushButton
from PyQt5.QtCore import QThread, QMutex, QWaitCondition, pyqtSignal, QObject
import subprocess

class Trigger(QObject):
    signal = pyqtSignal(str)

class GPT2_Launcher(QThread):
    def __init__(self, trigger):
        QThread.__init__(self)
        self.trigger = trigger

    def definenet(self, name, waitcond, model_dir):
        self.dir = model_dir
        self.name = name
        self.waitcond = waitcond
        self.mutex = QMutex()
        self._input = ""
    
    def connectInput(self, inputTrigger):
        inputTrigger.signal.connect(self.setInput)

    def connectOutput(self, outputfunc):
        self.trigger.signal.connect(outputfunc)

    def setInput(self, _input):
        self._input = _input

    def run(self):
        while True:
            self.mutex.lock()
            self.waitcond.wait(self.mutex)
            ls = subprocess.run(["C:/Users/Chris Holmberg Bahns/AppData/Local/Programs/Python/Python37/python.exe", "./src/gpt2_model.py", self.name, self.dir, self._input], stdout=subprocess.PIPE)
            output = self._input + ls.stdout.decode('UTF-8')
            self.trigger.signal.emit(output)
            self.mutex.unlock()



        
