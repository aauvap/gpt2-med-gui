import tensorflow as tf
import model, sample, encoder
from PyQt5.QtCore import pyqtSignal, QObject

import argparse
import copy
import json 
import os
import numpy as np
import model, sample, encoder
import argparse

class Trigger(QObject):
    signal = pyqtSignal(str)

class GPT2_Network:
    trigger = Trigger()
    def defineNetwork(self, _model, _models_dir):
        ## Prepare Network
        self.Model_name = _model #Available Models {124M, 355M, 774M}
        self.seed=None 
        self.nsamples=1
        self.batch_size=1
        self.length=None
        self.temperature=1
        self.top_k=0
        self.top_k=1
        self.models_dir= _models_dir
        self.models_dir = os.path.expanduser(os.path.expandvars(self.models_dir))
        if self.batch_size is None:
            self.batch_size = 1
        assert self.nsamples % self.batch_size == 0

        self.enc = encoder.get_encoder(self.Model_name, self.models_dir)
        self.hparams = model.default_hparams()
        with open(os.path.join(self.models_dir, self.Model_name, 'hparams.json')) as f:
            self.hparams.override_from_dict(json.load(f))

        if self.length is None:
            self.length = self.hparams.n_ctx // 2
        elif self.length > self.hparams.n_ctx:
            raise ValueError("Can't get samples longer than window size: %s" % self.hparams.n_ctx)

        #start the network
        self.sess = tf.Session()
        self.graph = tf.Graph()
        #self.init = tf.initialize_all_variables()
        #self.graph.as_default()
        self.context = tf.placeholder(tf.int32, [self.batch_size, None])
        np.random.seed(self.seed)
        tf.set_random_seed(self.seed)
        self.output = sample.sample_sequence(
            hparams=self.hparams, 
            length=self.length,
            context=self.context,
            batch_size=self.batch_size,
            temperature=self.temperature, 
            top_k=self.top_k, 
            top_p=self.top_k
        )
        self.saver = tf.train.Saver()
        self.ckpt = tf.train.latest_checkpoint(os.path.join(self.models_dir, self.Model_name))
        self.saver.restore(self.sess, self.ckpt)
           
    def process_prompt(self, prompt, standalone):
        context_tokens = self.enc.encode(prompt)
        generated = 0
        for _ in range(self.nsamples // self.batch_size):
            out = self.sess.run(self.output, feed_dict={
                self.context: [context_tokens for _ in range(self.batch_size)]
            })[:, len(context_tokens):]
        for i in range(self.batch_size):
            generated += 1
            gpt2_text = self.enc.decode(out[i])

        if standalone:
            print(prompt + gpt2_text)
        else:
            self.trigger.signal.emit(prompt + gpt2_text)

    def stop(self):
        self.sess.close()

if __name__ == "__main__":
    #os.chdir("./../")
    parser = argparse.ArgumentParser()
    parser.add_argument("model", type=str, help="The name of the model [124M, 355M, 774M]")
    parser.add_argument("model_dir", type=str, help="path to the model directory")
    parser.add_argument("input", type=str, help="The input string that the model will process")
    args = parser.parse_args()

    network = GPT2_Network()
    network.defineNetwork(args.model, args.model_dir)
    network.process_prompt(args.input, True)

