#QT IMPORT
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QHBoxLayout, QTextEdit, QLabel, QVBoxLayout, QPushButton, QComboBox, QGroupBox, QProgressBar
from PyQt5.QtCore import QObject, pyqtSignal, QWaitCondition, QMutex, QThread
from PyQt5 import QtCore
import QtWaitingSpinner
from gpt2_model_seperate import GPT2_Launcher
from gpt2_model import GPT2_Network, Trigger
#GPT Import
import argparse
import copy
import json 
import os
import numpy as np
import tensorflow as tf
import model, sample, encoder
import threading
import queue

parser = argparse.ArgumentParser(description="GPT2 Text Generator")
parser.add_argument("--models_dir", type=str, default="C:/Code/gpt2-med-gui/models/", help="Path to directory with all the model folders")
args = parser.parse_args()


class OutputBox(QTextEdit):
    def __init__(self, index):
        QTextEdit.__init__(self)
        self.title = 'output'
        self.setReadOnly(True)
        self.verticalScrollBar()
        self.index = index
        
    
    def got_new_text(self, text):
        
        outProgressBars[self.index].hide()
        self.show()
        self.clear()
        self.setPlainText(text)

class InputBox(QTextEdit):
    def __init__(self, *args, **kwargs):
        QTextEdit.__init__(self)
        self.title = 'input'
        self.placeholderText = 'Input writing prompt'

class MenuBar(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.layout = QHBoxLayout()

        self.button_process = QPushButton("Fortsæt teksten", self)
        self.button_process.clicked.connect(self.process)

        self.waitingSpinner = QtWaitingSpinner.QtWaitingSpinner()
        self.layout.addWidget(self.button_process)
        self.layout.addWidget(self.waitingSpinner)
        self.setLayout(self.layout)

    def renable_button(self, text):
        self.waitingSpinner.stop()
        self.button_process.setEnabled(True)

    def process(self):
        #Handle input
        input_text = inputbox.toPlainText()
        self.button_process.setEnabled(False)
        self.waitingSpinner.start()
        
        for ob in outTextBoxes:
            ob.hide()
            ob.setPlainText("Genererer ny tekst...")

        for op in outProgressBars:
            op.setMaximum(0)
            op.show()

        inputTrigger.signal.emit(input_text)
        signaller.wakeAll()
        
        '''
        for m in active_model_threads:
            #m.process_prompt(input_text)
            nt = threading.Thread(target=m.process_prompt, args=[input_text])
            nt.start()
            '''

class PromptDropDownMenu(QComboBox):
    def __init__(self):
        QComboBox.__init__(self)
        #Options
        self.addItem("Skriv din egen tekst")
        self.addItem("Pakkeliste til Mars")
        self.addItem("Filmmanuskript")
        self.addItem("Kodeeksempel")
        self.addItem("Ringenes Herre")
        self.addItem("Madlavningsopskrift")
        self.addItem("Brev")
	
        #Get Prompts
        self.prompts = self.get_dict_of_prompts()
        #Initialize empty string
        inputbox.setPlainText("")
        #Connect function
        self.currentTextChanged.connect(self.selectionchange)
    
    def selectionchange(self):
        new_text=""
        new_text = self.prompts[str(self.currentText())]

        #Set selected text
        inputbox.clear()
        inputbox.setPlainText(new_text)
    
    def get_dict_of_prompts(self):
        prompts = dict()
        prompts = {
            "Skriv din egen tekst": str(""),
            "Pakkeliste til Mars": str("Before boarding your rocket to Mars, remember to pack these items"),
            "Filmmanuskript": str("Thor: The Tesseract belongs on Asgard, no human is a match for it.\n Tony turns to leave, but Steve stops him.\n Steve: You're not going alone!\n Tony: You gonna stop me?"),
            "Kodeeksempel": str("while (true) { \n // Get a single row \n var row = rows[i];"),
            "Ringenes Herre": str("Legolas and Gimli advanced on the orcs, raising their weapons with a harrowing war cry."),
            "Madlavningsopskrift": str("Making an omelette is simple! \n 1."),
            "Brev": str("Dear Penthouse: I never thought that I would be writing this, but sometimes, life can get pretty crazy.")
            }

        return prompts

class GPT2_Network_Thread(QThread):
    def inittf(self, _model, waitcond, _model_dir):
        self._model_dir = _model_dir
        self._model = _model
        self.waitcond = waitcond
        self.network = GPT2_Network()
        self.inputText = ""

    def setInput(self, _input):
        self.inputText = _input

    def connectInput(self, inputTrigger):
        inputTrigger.signal.connect(self.setInput)

    def connectOutput(self, outputFunc):
        self.network.trigger.signal.connect(outputFunc)

    def run(self):
        print("Running model {}".format(self._model))
        self.network.defineNetwork(self._model, self._model_dir)
        self.mutex = QMutex()
        while True:
            self.mutex.lock()
            self.waitcond.wait(self.mutex)
            self.network.process_prompt(self.inputText, False)
            self.mutex.unlock()

## Initial QT Stuff
app = QApplication([])
window = QWidget()
window.setWindowTitle("Automatisk forfatterrobot fra OpenAI")
window.setWindowFlag(QtCore.Qt.WindowCloseButtonHint, False)
layout = QVBoxLayout()

#Widgets
inputbox = InputBox()
menuBar = MenuBar()
promptDropDownMenu = PromptDropDownMenu()

#Layout
inputGroupBox = QGroupBox("Input")
inputGroupBoxLayout = QVBoxLayout()
inputGroupBoxLayout.addWidget(promptDropDownMenu)
inputGroupBoxLayout.addWidget(inputbox)
inputGroupBox.setLayout(inputGroupBoxLayout)

layout.addWidget(inputGroupBox)

layout.addWidget(menuBar)

outputGroupBox = QGroupBox("Automatisk genereret tekst")
outputGroupBoxLayout = QHBoxLayout()

#Create outputs
outGroupBoxboxes = [QGroupBox("Lille model (124 millioner parametre)"), 
                    QGroupBox("Større model (355 millioner parametre)")]

outTextBoxes = [OutputBox(0), OutputBox(1)]
outProgressBars = [QProgressBar(), QProgressBar()]

#Initialize model threads
signaller = QWaitCondition()
inputTrigger = Trigger()

#Initialize small 
lm = GPT2_Network_Thread()
lm.inittf("124M", signaller, args.models_dir)
lm.connectOutput(outTextBoxes[0].got_new_text)
lm.connectOutput(menuBar.renable_button)
lm.connectInput(inputTrigger)
lm.start()

#Add outputs
for idx, o in enumerate(outGroupBoxboxes):
    textBoxLayout = QVBoxLayout()
    textBoxLayout.addWidget(outProgressBars[idx])
    outProgressBars[idx].hide()
    textBoxLayout.addWidget(outTextBoxes[idx])

    o.setLayout(textBoxLayout)
    outputGroupBoxLayout.addWidget(o)

    #connect input output of seperate threads

outputGroupBox.setLayout(outputGroupBoxLayout)


layout.addWidget(outputGroupBox)
layout.setStretchFactor(outputGroupBox, 2)
layout.setStretchFactor(inputGroupBox, 1)

#Setlayout
window.setLayout(layout)

#Initialize medium
mTrigger = Trigger()
mm = GPT2_Launcher(mTrigger)
mm.definenet("355M", signaller, args.models_dir)
mm.connectOutput(outTextBoxes[1].got_new_text)
mm.connectInput(inputTrigger)
mm.start()

# #Initialize Large
# sTrigger = Trigger()
# sm = GPT2_Launcher(sTrigger)
# sm.definenet("774M", signaller, args.models_dir)
# sm.connectOutput(outTextBoxes[2].got_new_text)
# sm.connectInput(inputTrigger)
# sm.start()


#QT Applicatoin start
window.show()
app.exec_()
